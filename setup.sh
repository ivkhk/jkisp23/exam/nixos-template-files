#!/bin/bash
curl -o /tmp/disko.nix https://gitlab.com/ivkhk/jkisp23/exam/nixos-template-files/-/raw/main/disko.nix
nix \
  --experimental-features "nix-command flakes" \
  run github:nix-community/disko -- \
  --mode disko /tmp/disko.nix
nixos-generate-config --root /mnt
curl -o /mnt/etc/nixos/configuration.nix https://gitlab.com/ivkhk/jkisp23/exam/nixos-template-files/-/raw/main/configuration.nix
nixos-install --no-root-password --root /mnt
reboot
